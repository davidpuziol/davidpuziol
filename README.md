# Davidpuziol

<h1>I'm David Puziol Prata</h1>

<h2> 👨🏻‍💻 About Me </h2>

- 👨‍👧 &nbsp; Marina's father
- 🇧🇷 &nbsp; From Brazil
- 👨‍🎓 &nbsp; Computer Engineer
- 🤔 &nbsp; Exploring new automation technologies and fast software development solutions
- 💼 &nbsp; Working as a Team Leader DevOps
- 🕵 &nbsp; Learning more about Cloud Architecture, IaC, CI/CD, Monitoring, and Sofware Development
- 🧐 &nbsp; Hobby with Internet of Things technologies, home automation and 3D printing
- ☕ &nbsp; Coffee expert

<h3>🛠 Tech Stack</h3>

- ☁ &nbsp; AWS | Azure | GCP |
- 💻 &nbsp; Terraform | Packer | Ansible | Kubernetes | Docker | Vault
- 💻 &nbsp; Gitlab | GitHub
- 💻 &nbsp; Agile | GitOps
- 💻 &nbsp; Microservices | Kafka | Service Mesh

<h3>🛠 Internet of Things and Home Automation</h3>

- 💻 &nbsp; Home Assistant | Node Red
- 💻 &nbsp; Z-wave | Zibgee | MQTT | ESPHome | Tasmota
- 💻 &nbsp; Ubiquiti UniFi | UNMS
- 💻 &nbsp; Raspberry PI | Unraid | Synology | ProxMox

<br/>

<div align="center">

  <img align="center" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/vim/vim-original.svg">
  <img align="center" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/vscode/vscode-original.svg">
  <img align="center" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/git/git-original.svg">
  <img align="center" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/gitlab/gitlab-original.svg">
  <img align="center" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/jenkins/jenkins-original.svg">
  <img align="center" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/jira/jira-original.svg">
  <img align="center" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/confluence/confluence-original.svg">
  <img align="center" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/linux/linux-original.svg">
  <img align="center" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/docker/docker-original.svg">
  <img align="center" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/kubernetes/kubernetes-plain.svg">
  <img align="center" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/amazonwebservices/amazonwebservices-original.svg">
  <img align="center" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/azure/azure-original.svg">
  <img align="center" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/googlecloud/googlecloud-original.svg">
  <img align="center" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/packer/packer-original.svg">
   <img align="center" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/vagrant/vagrant-original.svg">
  <img align="center" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/terraform/terraform-original.svg">
  <img align="center" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/ansible/ansible-original.svg">
  <img align="center" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/argocd/argocd-original.svg">

<div align="center">
<br/>
  <h3> 🤝 Connect with Me </h3>
</p>

<div align="center">
  <a href="https://www.linkedin.com/in/davidpuziol" target="_blank"><img src="https://img.shields.io/badge/-LinkedIn-%230077B5?style=for-the-badge&logo=linkedin&logoColor=white" target="_blank"></a>
  <a href="https://www.buymeacoffee.com/davidpuziol" target="_blank"><img src="https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png" target="_blank"></a>
 	<a href="https://twitter.com/davidpprata" target="_blank"><img src="https://img.shields.io/badge/Twitter-1DA1F2?style=for-the-badge&logo=twitter&logoColor=white" target="_blank"></a><br/>
  <a href="mailto:davidpuziol@gmail.com"><img alt="Email" src="https://img.shields.io/badge/davidpuziol@gmail.com-blue?style=flat-square&logo=gmail"></a>
</p>